function nCr(n, r) {
	return fact(n) / (fact(r) * fact(n - r))
}

function fact(n) {
	x = 1;
	for (y = 2; y <= n; y++) {
		x = x * y;
	}
	return x;
}
console.log(nCr(6, 3));																															//where n = 6 ,r=3