function leapyear(year) {
	if (year % 4 == 0 && year % 100 != 0) {
		return "It is a leap year";
	}
	else if (year % 400 == 0)																												//year should be divisible by 400
	{
		return "It is a leap year";
	}
	else {
		return "It is not a leap year";
	}
}
console.log((leapyear(2021)));


